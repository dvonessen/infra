# Infrastructure

![GitLab last commit](https://img.shields.io/gitlab/last-commit/yggdrasil3/infra)
![GitLab language count](https://img.shields.io/gitlab/languages/count/yggdrasil3/infra)
![GitLab](https://img.shields.io/gitlab/license/yggdrasil3/infra)
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/yggdrasil3/infra?branch=main)
